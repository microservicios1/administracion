<html lang="es">
<head>
  <meta name="viewport" content="width=device-width">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Reportes</title>
  <link rel="stylesheet" type="text/css" href="StRod.css">
  <style>
    th
    {
      font-size: 18px;
      border: 1px solid black;
      text-align: center;
    }
    td
    {
      text-align: center;
      font-size: 18px;
      border: 1px solid black;
    }
  </style>
  <?php
    include 'dbc.php';
    include 'session.php';
    $conn = mysqli_connect($host,$user,$pass,$db);
    $cantidadesVM=array('entregadoVM' => 0,'solicitadoVM' => 0,'entregadoCPU' => 0,'solicitadoCPU' => 0,'entregadoRAM' => 0,'solicitadoRAM' => 0,'entregadoStatic' => 0,'solicitadoStatic' => 0,'entregadoShared' => 0,'solicitadoShared' => 0);
    //if(Datos vacios)
    if($_POST['Direccion']==""&&$_POST['Estatus']=="")
      $sql="select CPUEntregado,RAMEntregado,CPUSolicitado,RAMSolicitado,estatus,storageSolicitado,sharedSolicitado,storageEntregado,sharedEntregado from maquinas";
    else
    {
      $sql="";
      if($_POST['Direccion']!=""&&$_POST['Estatus']=="")
        $sql="select maquinas.estatus,maquinas.CPUSolicitado,maquinas.RAMSolicitado,maquinas.storageSolicitado,maquinas.sharedSolicitado,maquinas.CPUEntregado,maquinas.RAMEntregado,maquinas.storageEntregado,maquinas.sharedEntregado from proyectos left join maquinas on proyectos.folio=maquinas.folio where proyectos.direccionId=".$_POST['Direccion'];
      if($_POST['Direccion']==""&&$_POST['Estatus']!="")
        $sql="select maquinas.estatus,maquinas.CPUSolicitado,maquinas.RAMSolicitado,maquinas.storageSolicitado,maquinas.sharedSolicitado,maquinas.CPUEntregado,maquinas.RAMEntregado,maquinas.storageEntregado,maquinas.sharedEntregado from proyectos left join maquinas on proyectos.folio=maquinas.folio where maquinas.estatus='".$_POST['Estatus']."'";
      if($_POST['Direccion']!=""&&$_POST['Estatus']!="")
        $sql="select maquinas.estatus,maquinas.CPUSolicitado,maquinas.RAMSolicitado,maquinas.storageSolicitado,maquinas.sharedSolicitado,maquinas.CPUEntregado,maquinas.RAMEntregado,maquinas.storageEntregado,maquinas.sharedEntregado from proyectos left join maquinas on proyectos.folio=maquinas.folio where proyectos.direccionId=".$_POST['Direccion']." and maquinas.estatus='".$_POST['Estatus']."'";
    }
    $VMinfo = mysqli_query($conn,$sql);
    if(!$VMinfo)
      echo "Conexion con BD fallida o registro invalido";
    else
    {
      while($VMres = mysqli_fetch_array($VMinfo))
      {
        if($VMres['estatus']!="CANCELADO"||$_POST['Estatus']=="CANCELADO")
        {
          if($VMres['estatus']=="ENTREGADO")
            $cantidadesVM['entregadoVM']++;
          $cantidadesVM['solicitadoVM']++;
          $cantidadesVM['entregadoCPU'] += $VMres['CPUEntregado'];
          $cantidadesVM['solicitadoCPU'] += $VMres['CPUSolicitado'];
          $cantidadesVM['entregadoRAM'] += $VMres['RAMEntregado'];
          $cantidadesVM['solicitadoRAM'] += $VMres['RAMSolicitado'];
          $cantidadesVM['entregadoStatic'] += $VMres['storageEntregado'];
          $cantidadesVM['solicitadoStatic'] += $VMres['storageSolicitado'];
          $cantidadesVM['entregadoShared'] += $VMres['sharedEntregado'];
          $cantidadesVM['solicitadoShared'] += $VMres['sharedSolicitado'];
        }
      }
    }
  ?>
</head>
<body>
  <div class="container" >
    <!--      NAV      -->
      <ul id="nav">
        <li><a href="<?php echo $logout;?>">Cerrar sesion</a></li>
        <?php
          if($_COOKIE['userName']=='VY8G08A')
          {
            ?>
            <li><a href="<?php echo $consulk;?>">Spec Ops</a></li>
            <?php
          }
        ?>
        <li>User : <?php echo $_COOKIE['userName'];?></li>
        <!--<li><?php //echo "<script>var w = screen.width-60;var h=screen.height-140</script>"; echo "<a href=\"#\" onclick=\"window.open('".$showtables."','','menubar=0,titlebar=0,width='+w+',height='+h+',resizable=0,left=60px,top=40px')\" >Mostrar historial</a>";?></li>-->
        <li><a href="<?php echo $solicitudes;?>">Crear Solicitud</a></li>
        <li><a href="<?php echo $reporte;?>">Reportes</a></li>
        <li><a href="<?php echo $choose;?>">Solicitudes Actuales</a></li>
        <li clas="current"><a href="<?php echo $inside;?>">Proyectos</a></li>
      </ul>
    <br><br>  <br>
    <form method='post' action='callInfo.php' id='fist' >
    <br><br><br>
      <table align="center" width="70%">
        <tr>
          <th width="16%">
          </th>
          <th width="28%">
            Entregado:
          </th>
          <th width="28%">
            Pendiente:
          </th>
          <th width="28%">
            Total:
          </th>
        </tr>
        <tr>
          <th width="16%">
            VMs :
          </th>
          <td width="28%">
            <?php echo $cantidadesVM['entregadoVM']; ?>
          </td>
          <td width="28%">
            <?php echo $cantidadesVM['solicitadoVM']-$cantidadesVM['entregadoVM']; ?>
          </td>
          <td width="28%">
            <?php echo $cantidadesVM['solicitadoVM']; ?>
          </td>
        </tr>
        <tr>
          <th width="16%">
            vCPU :
          </th>
          <td width="28%">
            <?php echo $cantidadesVM['entregadoCPU']; ?>
          </td>
          <td width="28%">
            <?php echo $cantidadesVM['solicitadoCPU']-$cantidadesVM['entregadoCPU']; ?>
          </td>
          <td width="28%">
            <?php echo $cantidadesVM['solicitadoCPU']; ?>
          </td>
        </tr>
        <tr>
          <th width="16%">
            RAM :
          </th>
          <td width="28%">
            <?php echo $cantidadesVM['entregadoRAM']; ?>
          </td>
          <td width="28%">
            <?php echo $cantidadesVM['solicitadoRAM']-$cantidadesVM['entregadoRAM']; ?>
          </td>
          <td width="28%">
            <?php echo $cantidadesVM['solicitadoRAM']; ?>
          </td>
        </tr>
        <tr>
          <th width="16%">
            Storage :
          </th>
          <td width="28%">
            <?php echo $cantidadesVM['entregadoStatic']; ?>
          </td>
          <td width="28%">
            <?php echo $cantidadesVM['solicitadoStatic']-$cantidadesVM['entregadoStatic']; ?>
          </td>
          <td width="28%">
            <?php echo $cantidadesVM['solicitadoStatic']; ?>
          </td>
        </tr>
        <tr>
          <th width="16%">
            Disco Compartido:
          </th>
          <td width="28%">
            <?php echo $cantidadesVM['entregadoShared']; ?>
          </td>
          <td width="28%">
            <?php echo $cantidadesVM['solicitadoShared']-$cantidadesVM['entregadoShared']; ?>
          </td>
          <td width="28%">
            <?php echo $cantidadesVM['solicitadoShared']; ?>
          </td>
        </tr>
      </table>
      <br><br>
      <table align="center" width="80%">
        <tr>
          <th width="50%">
            Direccion :  <select name="direccion" id="direccion" onchange="this.form.submit()" >
              <option value="">- Direccion -</option>
              <?php
                $conn = mysqli_connect($host, $user, $pass, $db);
                $re = mysqli_query($conn,"select direcciones.nombre,direcciones.direccionId,count(proyectos.direccionId) from direcciones left join proyectos on direcciones.direccionId=proyectos.direccionId group by direcciones.direccionId");
                $r=mysqli_affected_rows($conn);
                if($r<1)
                  echo "<option value=\"\">No disponible</option> ";
                else
                while($row = mysqli_fetch_array($re))
                {
                  $o ="<option ";
                  if($row['direccionId']==$_POST['Direccion'])
                    $o .= "selected ";
                  $o .= "value=\"".$row[1]."\">".$row[0]." solicitudes: ".$row[2]."</option>";
                  echo $o;
                }
              ?>
            </select>
          </th>
          <th width="50%">
            Estatus :  <select name="Estatus" id="Estatus" onchange="this.form.submit()" >
              <option value="" >- Estatus -</option>
              <option value="EN PROCESO" <?php if($_POST['Estatus'] == "EN PROCESO") echo " selected ";?> >EN PROCESO</option>
              <option value="PENDIENTE" <?php if($_POST['Estatus'] == "PENDIENTE") echo " selected ";?> >PENDIENTE</option>
              <option value="ENTREGADO" <?php if($_POST['Estatus'] == "ENTREGADO") echo " selected ";?> >ENTREGADO</option>
              <option value="CANCELADO" <?php if($_POST['Estatus'] == "CANCELADO") echo " selected ";?> >CANCELADO</option>
            </select>
          </th>
        </tr>
      </table>
    </form>
  </div>
</body>
<script type="text/javascript">
  function rev(event)
  {
    var k = (event.which) ? event.which : event.keyCode;
    if ((k > 47 && k < 58)||(k > 64 && k < 91)||(k > 96 && k < 123)||(k == 160)||(k == 95)||(k == 45) ||(k == 44) ||(k ==32) )
      return true;
    else
      return false;
  }
  function isFloat(evt)
  {
    var ch = (evt.which) ? evt.which : event.keyCode
    if (ch > 31 &&(ch<48||ch>57)&&(ch!=46))
      return false;
    return true;
  }
</script>
</html>