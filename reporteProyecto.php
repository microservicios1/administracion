<?php
  include 'dbc.php';
  $conn = mysqli_connect($host,$user,$pass,$db);
  $sql="select proyectos.fecha,proyectos.proyecto,proyectos.solicitudPedido,proyectos.criticidad,proyectos.horario,proyectos.nUser,proyectos.ingresos,proyectos.disponibilidad,proyectos.inversion,proyectos.moneda,proyectos.hardwareLiberado,proyectos.cantidadesHW,proyectos.productoHW,proyectos.SOLPEHW,proyectos.fileHardware,proyectos.licenciasLiberado,proyectos.cantidadesL,proyectos.productoL,proyectos.SOLPEL,proyectos.fileLicencias,proyectos.descripcion,persona.nombre,persona.gerencia,persona.direccionId,persona.celular,persona.extension,persona.correo,persona.cargo from proyectos left join persona on proyectos.solicita=persona.userId where folio='".$_GET['folio']."'";
  $re=mysqli_query($conn,$sql);
  $thatData = mysqli_fetch_array($re);
  $direccion="";
  if($thatData['direccionId']>0)
  {
    $sql2="select nombre from direcciones where direccionId=".$thatData['direccionId'];
    $re2=mysqli_query($conn,$sql2);
    $data = mysqli_fetch_array($re2);
    $direccion=$data['nombre'];
  }
  $n1=0;
  $sql2="select maquinas.CPUSolicitado,maquinas.RAMSolicitado,maquinas.storageSolicitado,maquinas.netcard,especificacionesSO.SOSolicitado,especificacionesDB.base from maquinas left join especificacionesSO on especificacionesSO.folioNumber=maquinas.interId left join especificacionesDB on especificacionesDB.folioNumber=maquinas.interId where maquinas.folio='".$_GET['folio']."'";
  $re2=mysqli_query($conn,$sql2);
  $m1=mysqli_affected_rows($conn);
  if($m1==""||$m1==0)
  {
    $m1=1;
    $n1=1;
  }
  require('ToRtf.php');
  $f = new ToRtf();
  $f->fichero = 'the-other-images/COMITE/COMITE'.$m1.'.rtf';
  $f->fsalida = 'Reporte_COMITE.doc';
  $f->dirsalida = '';
  $f->retorno = 'fichero';
  $f->prefijo = '';
  $f->valores = array(
    '#*FECHA*#' => $thatData['fecha'],
    '#*FOLIO*#' => $_GET['folio'],
    '#*PROYECTO*#' => $thatData['proyecto'],
    '#*GERENCIA*#' => $thatData['gerencia'],
    '#*AREA*#' => $direccion,
    '#*NOMBRE*#' => $thatData['nombre'],
    '#*CELULAR*#' => $thatData['celular'].'/'.$thatData['extension'],
    '#*CARGO*#' => $thatData['cargo'],
    '#*CORREO*#' => $thatData['correo'],
    '#*SOLICITUD*#' => $thatData['solicitudPedido'],
    '#*INVERSION*#' => $thatData['inversion'],
    '#*MONEDA*#' => $thatData['moneda'],
    '#*PLTFNV*#' =>  'si/no',
    '#*HWCANT*#' => $thatData['cantidadesHW'],
    '#*HWPRODUCT*#' => $thatData['productoHW'],
    '#*LCCANT*#' => $thatData['cantidadesL'],
    '#*LCPRODUCT*#' => $thatData['productoL'],
    '#*DESCRIPCION*#' => $thatData['descripcion'],
    '#*HROP*#' => $thatData['horario'],
    '#*NUSR*#' => $thatData['nUser'],
    '#*INGR*#' => $thatData['ingresos'],
    '#*DISPREQ*#' => $thatData['disponibilidad']
	);
  if($thatData['hardwareLiberado']==1)
    $f->valores['#*HWLIB*#'] = 'Si';
  else
    $f->valores['#*HWLIB*#'] = 'No';
  if($thatData['SOLPEHW']==1)
    $f->valores['#*HWSOLPE*#'] = 'Incluido en SOLPE';
  else if($thatData['SOLPEHW']==2)
    $f->valores['#*HWSOLPE*#'] = 'Pedido de soporte';
  else
    $f->valores['#*HWSOLPE*#'] = 'NA';
  if($thatData['licenciasLiberado']==1)
    $f->valores['#*LCLIB*#'] = 'Si';
  else
    $f->valores['#*LCLIB*#'] = 'No';
  if($thatData['SOLPEL']==1)
    $f->valores['#*LCSOLPE*#'] = 'Incluido en SOLPE';
  else if($thatData['SOLPEL']==2)
    $f->valores['#*LCSOLPE*#'] = 'Pedido de soporte';
  else
    $f->valores['#*LCSOLPE*#'] = 'NA';
  switch($thatData['criticidad'])
  {
    case "Critica":
      $f->valores['#*C*#'] = 'X';
      $f->valores['#*A*#'] = '';
      $f->valores['#*M*#'] = '';
      $f->valores['#*B*#'] = '';
    break;
    case "Alta":
      $f->valores['#*C*#'] = '';
      $f->valores['#*A*#'] = 'X';
      $f->valores['#*M*#'] = '';
      $f->valores['#*B*#'] = '';
    break;
    case "Media":
      $f->valores['#*C*#'] = '';
      $f->valores['#*A*#'] = '';
      $f->valores['#*M*#'] = 'X';
      $f->valores['#*B*#'] = '';
    break;
    case "Baja":
      $f->valores['#*C*#'] = '';
      $f->valores['#*A*#'] = '';
      $f->valores['#*M*#'] = '';
      $f->valores['#*B*#'] = 'X';
    break;
    default:
      $f->valores['#*C*#'] = '';
      $f->valores['#*A*#'] = '';
      $f->valores['#*M*#'] = '';
      $f->valores['#*B*#'] = '';
    break;
  }
  $a2=0;
  while($machineData = mysqli_fetch_array($re2))
  {
    $f->valores['#*CPU'.$a2.'*#'] = $machineData['CPUSolicitado'];
    $f->valores['#*RAM'.$a2.'*#'] = $machineData['RAMSolicitado'];
    $f->valores['#*HDD'.$a2.'*#'] = $machineData['storageSolicitado'];
    $f->valores['#*SO'.$a2.'*#'] = $machineData['SOSolicitado'];
    $f->valores['#*BD'.$a2.'*#'] = $machineData['base'];
    $f->valores['#*NINT'.$a2.'*#'] = $machineData['netcard'];
    $a2=$a2+1;
  }
  $f->valores['#*STSA*#'] = '';
  $f->valores['#*STSR*#'] = '';
  $f->valores['#*STSC*#'] = '';
  $f->valores['#*STSP*#'] = '';
  $f->rtf();
?>