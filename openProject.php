<html lang="es">
<head>
  <meta name="viewport" content="width=device-width">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Administrar proyecto</title>
  <link rel="stylesheet" type="text/css" href="StRod.css">
  <style>
   #folio,#fecha,#criticidad,#moneda,#adm,#proyect,#gerencia,#direccionId,#cantidadesHW,#cantidadesL,#solicitudPedido,#inversion
    {
      width:200px;
      float:right;
      margin-right:70px;
      font-size: 14px;
    }
    .diagramFormat
    {
      position:relative;
      top: -50px;
      cursor: pointer;
      background-color: #86c3d9;
      line-height: 2.2em;
      padding: 0.3em 8px;
      border: 1px solid #666;
      font-family: courier;
      border-radius: 0.5em;
      box-shadow: inset 0 0 0.1em #fff, 0.2em 0.2em 0.2em rgba( 0, 0, 0, 0.3 );
    }
    .container2
    {
      padding: 6px ;
      display: inline;
      margin-left: 30%;
    }
    table.minimalistBlack 
    {
      border: 3px solid #000000;
      width: 100%;
      text-align: center;
      border-collapse: collapse;
    }
    table.minimalistBlack td, table.minimalistBlack th 
    {
      border: 1px solid #000000;
      padding: 5px 4px;
    }
    table.minimalistBlack tbody td 
    {
      font-size: 15px;
    }
    table.minimalistBlack thead th 
    {
      font-size: 18px;
      font-weight: bold;
      color: #000000;
      text-align: left;
    }
    table.page {
      border-collapse: collapse; 
      width: 100%;
      text-align: center;
    }
    /* And this to your table's `td` elements. */
    #page td {
      padding: 0; 
      margin: 0;
    }
  </style>
  <?php
    include 'dbc.php';
    include 'session.php';
    $conn = mysqli_connect($host,$user,$pass,$db);
    if($_POST['folio']=="")
      header('Location: '.$index);
    $cantidadesVM=array('entregadoVM' => 0,'solicitadoVM' => 0,'entregadoCPU' => 0,'solicitadoCPU' => 0,'entregadoRAM' => 0,'solicitadoRAM' => 0,'entregadoStatic' => 0,'solicitadoStatic' => 0,'entregadoShared' => 0,'solicitadoShared' => 0);
    $sql="select interId,CPUEntregado,RAMEntregado,CPUSolicitado,RAMSolicitado,estatus,storageSolicitado,sharedSolicitado,storageEntregado,sharedEntregado from maquinas where folio='".$_POST['folio']."'";
    $VMinfo = mysqli_query($conn,$sql);
    if(!$VMinfo)
      echo "Conexion con BD fallida o registro invalido";
    else
    {
      while($VMres = mysqli_fetch_array($VMinfo))
      {
        if($VMres['estatus']=="ENTREGADO")
          $cantidadesVM['entregadoVM']++;
        $cantidadesVM['solicitadoVM']++;
        $cantidadesVM['entregadoCPU'] += $VMres['CPUEntregado'];
        $cantidadesVM['solicitadoCPU'] += $VMres['CPUSolicitado'];
        $cantidadesVM['entregadoRAM'] += $VMres['RAMEntregado'];
        $cantidadesVM['solicitadoRAM'] += $VMres['RAMSolicitado'];
        $cantidadesVM['entregadoStatic'] += $VMres['storageEntregado'];
        $cantidadesVM['solicitadoStatic'] += $VMres['storageSolicitado'];
        $cantidadesVM['entregadoShared'] += $VMres['sharedEntregado'];
        $cantidadesVM['solicitadoShared'] += $VMres['sharedSolicitado'];
      }
    }
    $sql="select * from proyectos where folio='".$_POST['folio']."'";
    $regFiltro = mysqli_query($conn,$sql);
    if(!$regFiltro)
      echo "Conexion con BD fallida o registro inexistente";
    else
      $thatData = mysqli_fetch_array($regFiltro);
  ?>
</head>
<body>
  <div class="container" >
    <!--      NAV      -->
    <ul id="nav">
      <li><a href="<?php echo $logout;?>">Cerrar sesion</a></li>
      <?php
        if($_COOKIE['userName']=='VY8G08A')
        {
          ?>
          <li><a href="<?php echo $consulk;?>">Spec Ops</a></li>
          <?php
        }
      ?>
      <li>User : <?php echo $_COOKIE['userName'];?></li>
      <!--<li><?php //echo "<script>var w = screen.width-60;var h=screen.height-140</script>"; echo "<a href=\"#\" onclick=\"window.open('".$showtables."','','menubar=0,titlebar=0,width='+w+',height='+h+',resizable=0,left=60px,top=40px')\" >Mostrar historial</a>";?></li>-->
      <li><a href="<?php echo $solicitudes;?>">Crear Solicitud</a></li>
      <li><a href="<?php echo $reporte;?>">Reportes</a></li>
      <li><a href="<?php echo $choose;?>">Solicitudes Actuales</a></li>
      <li clas="current"><a href="<?php echo $inside;?>">Proyectos</a></li>
    </ul>
    <form method='post' id="thatform" action='judement.php' >
      <br><br><br>
      <table width="100%">
        <tr>
          <td width="5%"></td>
          <td width="35%">
            Fecha de recepción : <input type="text" name="fecha" id="fecha" size="5"  value="<?php echo $thatData['fecha']; ?>" readonly><br>
            <input type="hidden" name="folio" id="folio" value="<?php echo $_POST['folio'];?>"><br>
            Proyecto : <input type="text" name="proyect" readonly id="proyect" value="<?php echo $thatData['proyecto']; ?>" ><br>
            <input type="hidden" name="proyecto" id="proyecto" value="<?php echo $thatData['proyecto']; ?>"><br>
            <!-- Gerencia -->
              Gerencia: <input type="text" name="gerencia" id="gerencia" required onkeypress="return rev(event)" value="<?php echo $thatData['gerencia']; ?>" autocomplete="off">
              <br><br>
            <!-- Direccion -->
              Dirección: <select name="direccionId" id="direccionId" required onchange="callTheAdmin(this)">
                <option value=""></option>
                <?php
                  $re = mysqli_query($conn,"select * from direcciones");
                  if(! $re)
                    echo "<option value=\"Pendiente\">Pendiente</option> ";
                  else
                  {
                    while($row = mysqli_fetch_array($re))
                    {
                      $o ="<option ";
                      if($thatData['direccionId'] == $row['direccionId'])
                        $o.=" selected ";
                      $o.="value=\"".$row['direccionId']."\">".$row['nombre']."</option>";
                      echo $o;
                    }
                    unset($o);
                    unset($re);
                  }
                  mysqli_close($conn);
                ?>
              </select><br><br>
            <!-- administrador -->
              Administrador : <input type="text" name="adm" readonly id="adm" value="<?php echo $thatData['administra']; ?>" >
              <input type="hidden" name="administra" id="administra" value="<?php echo $thatData['administra']; ?>">
            <!-- Criticidad  -->
              <br><br>
              Nivel de criticidad: <select name="criticidad" id="criticidad" required>
                <option <?php if($thatData['criticidad'] == ''){echo("selected");}?> value=""></option>
                <option <?php if($thatData['criticidad'] == 'Baja'){echo("selected");}?> value="Baja">Baja</option>
                <option <?php if($thatData['criticidad'] == 'Media'){echo("selected");}?> value="Media">Media</option>
                <option <?php if($thatData['criticidad'] == 'Alta'){echo("selected");}?> value="Alta">Alta</option>
                <option <?php if($thatData['criticidad'] == 'Critica'){echo("selected");}?> value="Critica">Critica</option>
              </select>
          </td>
          <td width="10%">
          </td>
          <td width="40%">
            <table class="minimalistBlack">
              <tr>
                <th width="25%">
                </th>
                <th width="25%">
                  Entregado:
                </th>
                <th width="25%">
                  Pendiente:
                </th>
                <th width="25%">
                  Total:
                </th>
              </tr>
              <tr>
                <th width="25%">
                  VMs :
                </th>
                <td width="25%">
                  <?php echo $cantidadesVM['entregadoVM']; ?>
                </td>
                <td width="25%">
                  <?php echo $cantidadesVM['solicitadoVM']-$cantidadesVM['entregadoVM']; ?>
                </td>
                <td width="25%">
                  <?php echo $cantidadesVM['solicitadoVM']; ?>
                </td>
              </tr>
              <tr>
                <th width="25%">
                  vCPU :
                </th>
                <td width="25%">
                  <?php echo $cantidadesVM['entregadoCPU']; ?>
                </td>
                <td width="25%">
                  <?php echo $cantidadesVM['solicitadoCPU']-$cantidadesVM['entregadoCPU']; ?>
                </td>
                <td width="25%">
                  <?php echo $cantidadesVM['solicitadoCPU']; ?>
                </td>
              </tr>
              <tr>
                <th width="25%">
                  RAM :
                </th>
                <td width="25%">
                  <?php echo $cantidadesVM['entregadoRAM']; ?>
                </td>
                <td width="25%">
                  <?php echo $cantidadesVM['solicitadoRAM']-$cantidadesVM['entregadoRAM']; ?>
                </td>
                <td width="25%">
                  <?php echo $cantidadesVM['solicitadoRAM']; ?>
                </td>
              </tr>
              <tr>
                <th width="25%">
                  Storage :
                </th>
                <td width="25%">
                  <?php echo $cantidadesVM['entregadoStatic']; ?>
                </td>
                <td width="25%">
                  <?php echo $cantidadesVM['solicitadoStatic']-$cantidadesVM['entregadoStatic']; ?>
                </td>
                <td width="25%">
                  <?php echo $cantidadesVM['solicitadoStatic']; ?>
                </td>
              </tr>
              <tr>
                <th width="25%">
                  Disco Compartido:
                </th>
                <td width="25%">
                  <?php echo $cantidadesVM['entregadoShared']; ?>
                </td>
                <td width="25%">
                  <?php echo $cantidadesVM['solicitadoShared']-$cantidadesVM['entregadoShared']; ?>
                </td>
                <td width="25%">
                  <?php echo $cantidadesVM['solicitadoShared']; ?>
                </td>
              </tr>
            </table>
          </td>
          <td width="10%">
          </td>
        </tr>
      </table>
      <br>
      <table width="100%">
        <tr>
          <td width="5%"></td>
          <td width="10%">
            Hardware liberado:<input type="checkbox" id="hardwareLiberado" name="hardwareLiberado" <?php if($thatData['hardwareLiberado'] == "1"){echo("checked");}?>><br>
            Licencias liberadas:<input type="checkbox" id="licenciasLiberado" name="licenciasLiberado" <?php if($thatData['licenciasLiberado'] == "1"){echo("checked");}?>>
          </td>
          <td width="2%"></td>
          <td width="23%">
            Solicitud Pedido : <input type="text" name="solicitudPedido" id="solicitudPedido" onkeypress="return rev(event)" value="<?php echo $thatData['solicitudPedido']; ?>" autocomplete="off"><br><br>
            Inversion : <input type="text" name="inversion" id="inversion" onkeypress="return isFloat(event)" value="<?php echo $thatData['inversion']; ?>" autocomplete="off"><br><br>
            Moneda: <select name="moneda" id="moneda" >
              <option <?php if($thatData['moneda'] == ''){echo("selected");}?> value=""></option>
              <?php
                $re = array('Peso(Mx)','Dolar(USA)','Dolar(CAN)','EURO');
                  for($i=0;$i<sizeof($re);$i++)
                  {
                    $o ="<option ";
                    if($thatData['moneda'] == $re[$i])
                      $o.=" selected ";
                    $o.="value=\"".$re[$i]."\">".$re[$i]."</option>";
                    echo $o;
                  }
                  unset($o);
                  unset($re);
              ?>
            </select>   
          </td>
          <td width="10%"></td>
          <td width="40%">
            <table width=100% class="minimalistBlack">
              <tr>
                <th width="25%">Horario de<br>operacion</th>
                <th width="25%">Numero de<br>usuarios</th>
                <th width="25%">Ingresos</th>
                <th width="25%">Disponibilidad<br>requerida</th>
              </tr>
              <tr>
                <td width="25%"><input type="text" width="100%" name="horario" id="horario" onkeypress="return rev(event)" autocomplete="off" value="<?php echo $thatData['horario']; ?>" required></td>
                <td width="25%"><input type="text" width="100%" name="nUser" id="nUser" onkeypress="return rev(event)" autocomplete="off" value="<?php echo $thatData['nUser']; ?>" required></td>
                <td width="25%"><input type="text" width="100%" name="ingresos" id="ingresos" onkeypress="return rev(event)" autocomplete="off" value="<?php echo $thatData['ingresos']; ?>" required></td>
                <td width="25%"><input type="text" width="100%" name="disponibilidad" id="disponibilidad" onkeypress="return rev(event)" autocomplete="off" value="<?php echo $thatData['disponibilidad']; ?>" required></td>
              </tr>
            </table>
          </td>
          <td width="10%"></td>
        </tr>
      </table>
      <br>
      <table width="100%">
        <tr>
        <th width="4%"></th>
        <td width="20%">Descripcion de proyecto :<br><textarea style="align: center;" rows="6" cols="48" name="descripcion" id="descripcion" onkeypress="return rev(event)" required autocomplete="off" ><?php echo $thatData['descripcion']; ?></textarea></td>
        <td width="2%"></td>
        <td width="20%">Comentarios :<br><textarea style="align: center;" rows="6" cols="48" name="comentarios" id="comentarios" onkeypress="return rev(event)" autocomplete="off" ><?php echo $thatData['comentarios']; ?></textarea></td>
        <th width="8%"></th>
        <th width="40%">
          <table class="page">
            <!--  Maquinas  -->
            <tr>
              <th width="40%">
                <a href="reporteProyecto.php?folio=<?php echo $_POST['folio']; ?>">Generar Documento</a><br><p></p><br>
              </th>
              <td width="60%">
                <input type="button" style="margin-left:5%;margin-right:5%;" name="consulMachine" id="consulMachine" onclick="window.open('<?php echo $maquinasCleaner; ?>?folio=<?php echo $_POST['folio']; ?>','TotallyNotNM','menubar=0,titlebar=0,width=1780,height=550,resizable=no,left=40px,top=80px')" value="Maquinas en Proyecto"/><br><p></p><br>
              </td>
            </tr>
            <!-- Botones Especiales -->
            <tr>
              <th width="40%">
                <input type="button" style="margin-right:6%;" name="datosSolicitante" id="datosSolicitante" onclick="window.open('showPersona.php?persona=<?php echo $thatData['solicita'];?>','Datos del Solicitante','menubar=0,titlebar=0,width=600,height=250,resizable=no,left=400px,top=80px')" value="Datos solicitante">
                <input type="hidden" name="solicita" id="solicita" value="<?php echo $thatData['solicita'];?>">
              </th>
              <td width="60%">
                <input type="submit" value="Modificar Datos"/>
              </td>
            </tr>
            <tr>
              <th colspan="2" style="text-align:center;">
                <br><br><br><br>
                <label for="diagrama" class="diagramFormat">
                  <span id="gummytext">Diagrama Actual</span>
                </label>
                <input type="button" style="opacity: 0; z-index: -1;" name="diagrama" id="diagrama" onclick="window.open('moodMap.php?solic=<?php echo $_POST['folio'];?>&no=1','Here you change the map','menubar=0,titlebar=0,width=450,height=450,resizable=no,left=120px,top=40px')" >
              </th>
            </tr>
          </table>
        </th>
        <th width="6%"></th>
        </tr>
      </table>
      <table width="100%">
        <tr>
          <td width="8%"></td>
          <td width="20%">
            <div id="hideHardware" <?php if($thatData['hardwareLiberado']!="1") echo "style=\"display:none;\"";?>>
              Hardware:<br><br> 
              Cantidades: <input type="number" name="cantidadesHW"  id="cantidadesHW" onkeypress="return rev(event)" value="<?php echo $thatData['cantidadesHW']; ?>" autocomplete="off"><br>
              Productos:<br>
              <textarea rows="3" cols="35" name="productoHW" id="productoHW" onkeypress="return rev(event)" autocomplete="off" ><?php echo $thatData['productoHW']; ?></textarea><br><br>
            </div>
          </td>
          <td width="20%">
            <div id="hideHardware2" <?php if($thatData['hardwareLiberado']!="1") echo "style=\"display:none;\"";?>>
              <br><br>
              <label for="opc1">&nbsp;&nbsp;Incluido en SOLPE:&nbsp;&nbsp; </label><input type="radio" name="SOLPEHW" id="opc1" value="1" <?php if($thatData['SOLPEHW'] == '1'){echo("checked");}?>><br>
              <label for="opc2">&nbsp;&nbsp;Pedido de soporte:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label><input type="radio" name="SOLPEHW" id="opc2" value="2" <?php if($thatData['SOLPEHW'] == '2'){echo("checked");}?>><br><br><br><br>
              <label for="fileHardware" class="diagramFormat"><span id="gummytext">Archivo Hardware Actual</span></label><input type="button" style="opacity: 0; z-index: -1;" name="fileHardware" id="fileHardware" onclick="window.open('moodMap.php?solic=<?php echo $_POST['folio'];?>&no=2','cambiar archivo Hardware','menubar=0,titlebar=0,width=450,height=450,resizable=no,left=120px,top=40px')" >
            </div>
          </td>
          <td width="5%"></td>
          <td width="20%">
            <div id="hideLicencias" <?php if($thatData['licenciasLiberado']!="1") echo "style=\"display:none;\"";?>>
              Licencias:<br><br>
              Cantidades: <input type="number" name="cantidadesL"  id="cantidadesL" onkeypress="return rev(event)" value="<?php echo $thatData['cantidadesL']; ?>" autocomplete="off"><br>
              Productos:<br>
              <textarea rows="3" cols="35" name="productoL" id="productoL" onkeypress="return rev(event)" autocomplete="off" ><?php echo $thatData['productoL']; ?></textarea><br><br>
            </div>
          </td>
          <td width="20%">
            <div id="hideLicencias2" <?php if($thatData['licenciasLiberado']!="1") echo "style=\"display:none;\"";?>>
              <br><br>
              <label for="opc1">&nbsp;&nbsp;Incluido en SOLPE:&nbsp;&nbsp; </label><input type="radio" name="SOLPEL" id="opc1" value="1" <?php if($thatData['SOLPEL'] == '1'){echo("checked");}?>><br>
              <label for="opc2">&nbsp;&nbsp;Pedido de soporte:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label><input type="radio" name="SOLPEL" id="opc2" value="2" <?php if($thatData['SOLPEL'] == '2'){echo("checked");}?>><br><br><br><br>
              <label for="fileLicencias" class="diagramFormat"><span id="gummytext">Archivo Licencias Actual</span></label><input type="button" style="opacity: 0; z-index: -1;" name="fileLicencias" id="fileLicencias" onclick="window.open('moodMap.php?solic=<?php echo $_POST['folio'];?>&no=3','cambiar archivo Licencias','menubar=0,titlebar=0,width=450,height=450,resizable=no,left=120px,top=40px')" >
            <div>
          </td>
          <td width="7%"></td>
        </tr>
      </table>
    </form>
  </div>
</body>
<script type="text/javascript">
  document.getElementById('hardwareLiberado').onchange=function()
  {
    if(this.checked)
    {
      document.getElementById('hideHardware').style ="";
      document.getElementById('hideHardware2').style ="";
    }
    else
    {
      document.getElementById('hideHardware').style ="display:none;";
      document.getElementById('hideHardware2').style ="display:none;";
    }
  }
  document.getElementById('licenciasLiberado').onchange=function()
  {
    if(this.checked)
    {
      document.getElementById('hideLicencias').style = "";
      document.getElementById('hideLicencias2').style = "";
    }
    else 
    {
      document.getElementById('hideLicencias').style = "display:none;";
      document.getElementById('hideLicencias2').style = "display:none;";
    }   
  }
  function rev(event)
  {
    var k = (event.which) ? event.which : event.keyCode;
    if ((k > 47 && k < 58)||(k > 64 && k < 91)||(k > 96 && k < 123)||(k == 160)||(k == 95)||(k == 45) ||(k == 44) ||(k ==32) )
      return true;
    else
      return false;
  }
  function isFloat(evt)
  {
    var ch = (evt.which) ? evt.which : event.keyCode
    if (ch > 31 &&(ch<48||ch>57)&&(ch!=46))
      return false;
    return true;
  }
  function dothathing(input)
  {
    var fileName = input.value.split('/').pop().split('\\').pop();
    if(2<fileName.length)
      document.getElementById("gummytext").innerHTML = fileName;
    else
      document.getElementById("gummytext").innerHTML = "Diagrama de Arquitectura";
  }
  function doHardware(input)
  {
    var fileName = input.value.split('/').pop().split('\\').pop();
    if(2<fileName.length)
      document.getElementById("gummytext").innerHTML = fileName;
    else
      document.getElementById("gummytext").innerHTML = "Archivo Inventario HW ";
  }
  function doLicencias(input)
  {
    var fileName = input.value.split('/').pop().split('\\').pop();
    if(2<fileName.length)
      document.getElementById("gummytext").innerHTML = fileName;
    else
      document.getElementById("gummytext").innerHTML = "Archivo Licencias ";
  }
  function callTheAdmin(select)
  {
    if(select.value=="")
    {
      document.getElementById("adm").value="";
      document.getElementById("administra").value="";
    }
    else if(select.value<=4)
    {
      document.getElementById("adm").value="RODRIGO LOPEZ MARTINEZ";
      document.getElementById("administra").value="RODRIGO LOPEZ MARTINEZ";
    }
    else if(select.value<=10)
    {
      document.getElementById("adm").value="ANA LILIA ACEVEDO JURADO";
      document.getElementById("administra").value="ANA LILIA ACEVEDO JURADO";
    }
    else
    {
      document.getElementById("adm").value="POR DEFINIR";
      document.getElementById("administra").value="POR DEFINIR";
    }
  }
</script>
</html>