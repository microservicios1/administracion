<html>
<head>
  <title>Respuesta</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link rel="stylesheet" type="text/css" href="StRod.css">
  <?php
    include 'dbc.php';
    include 'session.php';
    $conn = mysqli_connect($host, $user, $pass, $db);
    if(! $conn )
        die('Conexion sql fallida!');
  ?>
</head>
<body>
  <div class="container" align="center">
    <br>
    <!--      NAV      -->
      <ul id="nav">
        <li><a href="<?php echo $logout;?>">Cerrar sesion</a></li>
        <?php
          if($_COOKIE['userLvl']==1)
          {
            ?>
            <li><a href="<?php echo $consulk;?>">Spec Ops</a></li>
            <?php
          }
        ?>
        <li>User : <?php echo $_COOKIE['userName'];?></li>
        <li><?php echo "<script>var w = screen.width-60;var h=screen.height-140</script>"; echo "<a href=\"#\" onclick=\"window.open('".$showtables."','','menubar=0,titlebar=0,width='+w+',height='+h+',resizable=0,left=60px,top=40px')\" >Mostrar historial</a>";?></li>
        <li><a href="<?php echo $reporte;?>">Reportes</a></li>
        <li><a href="<?php echo $choose;?>">Solicitudes</a></li>
        <li><a href="<?php echo $index;?>">Proyectos</a></li>
      </ul>
    <br>
    <?php
      mysqli_autocommit($conn, FALSE);
      mysqli_begin_transaction($conn, MYSQLI_TRANS_START_READ_WRITE);
      $sql="select gerencia,direccionId,comentarios,descripcion,solicitudPedido,inversion,moneda,hardwareLiberado,cantidadesHW,productoHW,SOLPEHW,licenciasLiberado,cantidadesL,productoL,SOLPEL,administra from proyectos where folio='".$_POST['folio']."'";
      $some= array('direccionId','inversion','gerencia','comentarios','descripcion','solicitudPedido','moneda','administra');
      $re=mysqli_query($conn,$sql);
      $rowOfMaps = mysqli_fetch_array($re);
      $controlVariableSome=0;
      //   definir sql de proyectos 
      $sql="";
        for($j=0;$j<sizeof($some);$j++)
        {
          if($j<2)
          {
            if(!is_numeric($_POST[$some[$j]]))
              $temp=0;
            else
              $temp=$_POST[$some[$j]];
          }
          else
            $temp=$_POST[$some[$j]];
          if($temp!=$rowOfMaps[$some[$j]])
          {
            if($sql=="")
            {
              if($j<2)
                $sql="update proyectos set proyectos.".$some[$j]."=".$temp;
              else
                $sql="update proyectos set proyectos.".$some[$j]."='".$temp."'";
            }
            else
            {
              if($j<2)
                $sql .=",proyectos.".$some[$j]."=".$temp;
              else
                $sql .=",proyectos.".$some[$j]."='".$temp."'";
            } 
          }
        }
        if($_POST['hardwareLiberado']=="on")
        {
          if($rowOfMaps['hardwareLiberado']!=1)
          {
            if($sql=="")
              $sql ="update proyectos set proyectos.hardwareLiberado=1";
            else
              $sql .=",proyectos.hardwareLiberado=1";
          }
          if($rowOfMaps['cantidadesHW']!=$_POST['cantidadesHW'])
          {
            if($sql=="")
              $sql ="update proyectos set proyectos.cantidadesHW='".$_POST['cantidadesHW']."'";
            else
              $sql .=",proyectos.cantidadesHW='".$_POST['cantidadesHW']."'";
          } 
          if($rowOfMaps['productoHW']!=$_POST['productoHW'])
          {
            if($sql=="")
              $sql ="update proyectos set proyectos.productoHW='".$_POST['productoHW']."'";
            else
              $sql .=",proyectos.productoHW='".$_POST['productoHW']."'";
          }
          if($rowOfMaps['SOLPEHW']!=$_POST['SOLPEHW'])
          {
            if($_POST['SOLPEHW']==1||$_POST['SOLPEHW']==2)
            {
              if($sql=="")
                $sql ="update proyectos set proyectos.SOLPEHW=".$_POST['SOLPEHW'];
              else
                $sql .=",proyectos.SOLPEHW=".$_POST['SOLPEHW'];
            }
            else
            {
              if($sql=="")
                $sql ="update proyectos set proyectos.SOLPEHW=0";
              else
                $sql .=",proyectos.SOLPEHW=0";
            }
          }
        }
        else
        {
          if($rowOfMaps['hardwareLiberado']!=0)
          {
            if($sql=="")
              $sql ="update proyectos set proyectos.hardwareLiberado=0,proyectos.cantidadesHW='',proyectos.productoHW='',proyectos.SOLPEHW=0";
            else
              $sql .=",proyectos.hardwareLiberado=0,proyectos.cantidadesHW='',proyectos.productoHW='',proyectos.SOLPEHW=0";
          }
        }
        if($_POST['licenciasLiberado']=="on")
        {
          if($rowOfMaps['licenciasLiberado']!=1)
          {
            if($sql=="")
              $sql ="update proyectos set proyectos.licenciasLiberado=1";
            else
              $sql .=",proyectos.licenciasLiberado=1";
          }
          if($rowOfMaps['cantidadesL']!=$_POST['cantidadesL'])
          {
            if($sql=="")
              $sql ="update proyectos set proyectos.cantidadesL='".$_POST['cantidadesL']."'";
            else
              $sql .=",proyectos.cantidadesL='".$_POST['cantidadesL']."'";
          } 
          if($rowOfMaps['productoL']!=$_POST['productoL'])
          {
            if($sql=="")
              $sql ="update proyectos set proyectos.productoL='".$_POST['productoL']."'";
            else
              $sql .=",proyectos.productoL='".$_POST['productoL']."'";
          }
          if($rowOfMaps['SOLPEL']!=$_POST['SOLPEL'])
          {
            if($_POST['SOLPEL']==1||$_POST['SOLPEL']==2)
            {
              if($sql=="")
                $sql ="update proyectos set proyectos.SOLPEL=".$_POST['SOLPEL'];
              else
                $sql .=",proyectos.SOLPEL=".$_POST['SOLPEL'];
            }
            else
            {
              if($sql=="")
                $sql ="update proyectos set proyectos.SOLPEL=0";
              else
                $sql .=",proyectos.SOLPEL=0";
            }
          }
        }
        else
        {
          if($rowOfMaps['licenciasLiberado']!=0)
          {
            if($sql=="")
              $sql ="update proyectos set proyectos.licenciasLiberado=0,proyectos.cantidadesL='',proyectos.productoL='',proyectos.SOLPEL=0";
            else
              $sql .=",proyectos.licenciasLiberado=0,proyectos.cantidadesL='',proyectos.productoL='',proyectos.SOLPEL=0";
          }
        }
        if($sql!="")
        {
          $sql .= " where proyectos.folio='".$_POST['folio']."'";
          mysqli_query($conn,$sql);
          $r=mysqli_affected_rows($conn);
          if($r<1)
            $controlVariableSome=99;
        }
      //  commit o rollback
        if($controlVariableSome==0)
        {
          echo "Datos agregados .";
          //echo $sql;
          mysqli_commit($conn);
          mysqli_autocommit($conn, TRUE);
        }
        else
        {
          echo "Datos no agregados .Problema de conexion con base de datos!";
          //echo $sql;
          mysqli_rollback($conn);
          mysqli_autocommit($conn, TRUE);
        }  
      mysqli_close($conn);
    ?>
    <p>  </p><br>
  </div>
</body>
</html> 